# Caravanstalling Elst frontend application

This project is made with React framework.

The `deploy` directory contains the files needed for our review environments which were hosted on a kubernetes cluster.  The `manifest` project in the Gitlab group hosts the configuration and the agent that is deployed in the kubernetes cluster that is used in the deploy stage of this projects pipeline.

### Configuration of the app
The configuration of the app is done in the `.env` file in the root of the repository.
In this file you can specify the location of the API. For local development we recommend that you override these values in a `.env.local` file.
Both env-files get merged when starting a local development server (using `npm start`). That way u don't accidently commit a url to a local API.

## Pre-requirements

Make sure NodeJS is installed on your system. We have developed and tested this app with version v16.14.0.
Run `npm install` to install all dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

To learn React, check out the [React documentation](https://reactjs.org/).
