import { useState, useEffect } from "react";
import { removeEmptyValues } from "../components/shared/Utility";
import { makeRequest } from "./base";
import { IObject } from "./Models";

const useObjects = () => {
  const [state, setState] = useState({ data: [] as IObject[], loading: true });

  useEffect(() => {
    const fetchData = async () => {
      const rsp = await fetchAllObjects();
      setState({ data: rsp, loading: false });
    };
    fetchData();
  }, []);

  return state;
};

export const fetchAllObjects = async () => {
  const rsp = await fetch(process.env.REACT_APP_API_URL + "item");
  const parsedResponse: IObject[] = await rsp.json();
  parsedResponse.sort((a, b) => {
    return a.owner.lastName.localeCompare(b.owner.lastName, "en", {
      sensitivity: "base",
    });
  });
  return parsedResponse;
};

export const AddObject = (object: IObject) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(removeEmptyValues(object)),
  };
  return makeRequest(requestOptions, "item");
};

export const UpdateObject = (object: IObject) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(removeEmptyValues(object)),
  };
  return makeRequest(requestOptions, "item");
};

export const DeleteObject = (objectID: string) => {
  const requestOptions = {
    method: "DELETE",
  };
  return makeRequest(requestOptions, `item/${objectID}`);
};

export default useObjects;
