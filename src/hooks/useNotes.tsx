import { makeRequest } from "./base";

export const SaveNotes = (body: any) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: body,
  };
  return makeRequest(requestOptions, "save-note");
};
