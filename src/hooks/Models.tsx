interface IObject {
  id?: number;
  licensePlateNumber: string;
  length: number;
  type: string;
  bikeCarrier: boolean;
  owner: ICustomer;
  secondaryOwners?: ICustomer[];
  documents: IDocument[];
  notes?: INotes;
}

interface ICustomer {
  id?: number;
  firstName: string;
  lastName: string;
  street: string;
  houseNumber: string;
  postalCode: string;
  city: string;
  phoneNumber: string;
  email: string;
  documents: IDocument[];
  items?: IObject[];
  notes?: INotes;
}

interface IEmailTemplate {
  id: number | null | undefined;
  subject: string;
  emailType: string | undefined;
  htmlString: string;
  jsonTemplate: string;
}

interface IDocument {
    id: number,
    customersId?: number,
    itemId?: number,
    fileName: string,
    uploadDate: string,
    size: string
    fileString: string
}

interface INotes {
    id: number,
    note?: string,
    customerId?: number,
    itemId?: number
}

export type { IObject, ICustomer, IDocument, IEmailTemplate, INotes };
