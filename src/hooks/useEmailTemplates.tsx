import { useState, useEffect } from "react";
import { IEmailTemplate } from "./Models";

const useEmailTemplates = () => {
  const [state, setState] = useState({
    data: [] as IEmailTemplate[],
    loading: true,
  });

  useEffect(() => {
    const fetchData = async () => {
      const rsp = await fetch(
        process.env.REACT_APP_API_URL + "get-all-email-templates"
      );
      const parsedResponse: IEmailTemplate[] = await rsp.json();
      setState({ data: parsedResponse, loading: false });
    };
    fetchData();
  }, []);

  return state;
};

export default useEmailTemplates;
