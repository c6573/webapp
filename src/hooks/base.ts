export const makeRequest = (options: {}, params: string) => {
    return new Promise<any>(async (resolve, reject) => {
      await fetch(
        process.env.REACT_APP_API_URL +
          `${params}`,
        options
      )
        .then((response) => {
          if (response.status >= 400) {
              if (response.headers.get("content-type")?.startsWith("text")) {
                  response.text().then((txt) => reject(txt));
              } else if (response.headers.get("content-type")?.includes("json")) {
                  response.json().then((json) => reject(json.title));
              } else {
                  reject(response);
              }
          } else if (response.status === 200) {
              if (response.headers.get("content-type")?.startsWith("application/json")) {
                  resolve(response.json());
              } else {
                  resolve(response);
              }
          } else {
            resolve(response);
          }
        })
        .catch((error) => reject(error));
    });
  };
