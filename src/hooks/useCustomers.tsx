import { useState, useEffect } from "react";
import { removeEmptyValues } from "../components/shared/Utility";
import { makeRequest } from "./base";
import { ICustomer } from "./Models";

const useCustomers = () => {
  const [state, setState] = useState({
    data: [] as ICustomer[],
    loading: true,
  });

  useEffect(() => {
    const fetchData = async () => {
      const rsp = await fetchAllCustomers();
      setState({ data: rsp, loading: false });
    };
    fetchData();
  }, []);

  return state;
};

export const fetchAllCustomers = async () => {
  const rsp = await fetch(process.env.REACT_APP_API_URL + "person");
  const parsedResponse: ICustomer[] = await rsp.json();
  parsedResponse.sort((a, b) => {
    return a.lastName.localeCompare(b.lastName, "en", {
      sensitivity: "base",
    });
  });
  return parsedResponse;
};

export const AddCustomer = (customer: ICustomer) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(removeEmptyValues(customer)),
  };
  return makeRequest(requestOptions, "person");
};

export const UpdateCustomer = (customer: ICustomer) => {
  const requestOptions = {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(removeEmptyValues(customer)),
  };
  return makeRequest(requestOptions, "person");
};

export default useCustomers;
