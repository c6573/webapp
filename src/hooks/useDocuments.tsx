import { IDocument } from "./Models";
import { makeRequest } from "./base";

export const UploadDocument = (document: IDocument) => {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(document),
  };
  return makeRequest(requestOptions, "upload-file");
};

export const DeleteDocument = (id: string) => {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json" },
  };
  return makeRequest(requestOptions, "document/" + id);
};

export const DownloadDocument = (documentId: number) => {
  const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  return makeRequest(requestOptions, "document/" + documentId);
};
