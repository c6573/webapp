import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { generatePath, Link } from "react-router-dom";
import "./DetailViewNavBar.css";

type IDetailViewNavbar = {
  route: string;
  previous: undefined | number;
  current: undefined | string;
  next: undefined | number;
};

const DetailViewNavBar = (props: IDetailViewNavbar) => {
  return (
    <div className="nav mt-1">
      {props.previous === undefined ? (
        <button className="btn navbar-button disabled" aria-disabled>
          <BsChevronLeft />
        </button>
      ) : (
        <Link
          to={generatePath("/:route/:id", {
            route: props.route,
            id: props.previous.toString(),
          })}
          className="nav-link"
        >
          <BsChevronLeft />
        </Link>
      )}
      <span className="lead navbar-text">{props.current}</span>
      {props.next === undefined ? (
        <button className="btn navbar-button disabled" aria-disabled>
          <BsChevronRight />
        </button>
      ) : (
        <Link
          to={generatePath("/:route/:id", {
            route: props.route,
            id: props.next.toString(),
          })}
          className="nav-link"
        >
          <BsChevronRight />
        </Link>
      )}
    </div>
  );
};

export default DetailViewNavBar;
