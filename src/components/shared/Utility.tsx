export const removeEmptyValues = (obj: object): object => {
  return Object.entries(obj)
    .filter(([_, v]) => v !== "")
    .reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {});
};
