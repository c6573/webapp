import { MdSearch } from "react-icons/md";

type IDatagridUtilityBar = {
  title?: string;
  data: any[];
  setFilteredRows: (filteredRows: any[]) => void;
  input?: {
    title?: string;
    classes?: string;
    type: string;
    onChange: (e?: any) => void;
    hide?: boolean;
  };
  buttons?: {
    icon?: {};
    title?: string;
    onClick: (e?: any) => void;
    buttonClasses: string;
    disabled?: boolean;
    hide?: boolean;
  }[];
};

const DatagridUtilityBar = (props: IDatagridUtilityBar) => {
  function escapeRegExp(value: string): string {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  const requestSearch = (e: any) => {
    const filteredRows = props.data.filter((row: any) => {
      return testValuesInRow(row, e.target.value);
    });
    props.setFilteredRows(filteredRows);
  };

  const testValuesInRow = (row: any, value: any): boolean => {
    const searchRegex = new RegExp(escapeRegExp(value), "i");
    return Object.keys(row).some((field: any) => {
      if (row[field] !== null) {
        if (typeof row[field] === "object" && !Array.isArray(row[field])) {
          return testValuesInRow(row[field], value);
        } else {
          return searchRegex.test(row[field].toString());
        }
      }
    });
  };

  const buttons: any = [];
  props.buttons?.forEach((element, key) => {
    let classes = "btn me-1 " + element.buttonClasses;
    if (element.hide) classes += " hide";
    buttons.push(
      <button
        key={key}
        className={classes}
        onClick={element.onClick}
        disabled={element.disabled}
      >
        {element.icon}
        {element.title}
      </button>
    );
  });

  let input: any;
  if (props.input !== undefined) {
    let classes = props.input.classes;
    if (props.input.hide) classes += " hide";
    input = (
      <input
        className={classes}
        type={props.input.type}
        onChange={props.input.onChange}
      />
    );
  }

  return (
    <>
      <div className="row mt-4 mb-2 ">
        {props.title !== undefined && <h3 className="mb-3">{props.title}</h3>}
        <div className="col-xl-4 col-md-6 col-sm-7">
          <div className="input-group">
            <span className="input-group-text">
              <MdSearch />
            </span>
            <input
              type="search"
              className="form-control"
              placeholder="Search..."
              onChange={requestSearch}
            />
          </div>
        </div>
        <div
          key="button-list-1"
          className="col-xl-8 col-md-6 col-sm-5 d-inline-flex justify-content-end"
        >
          {input}
          {buttons}
        </div>
      </div>
    </>
  );
};

export default DatagridUtilityBar;
