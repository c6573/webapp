import { CircularProgress } from "@mui/material";

const LoadingIndicator = () => {
  return (
    <div className="d-flex mt-5 justify-content-center">
      <CircularProgress />
    </div>
  );
};

export default LoadingIndicator;
