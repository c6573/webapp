import { act, fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter, MemoryRouter } from "react-router-dom";
import ObjectsPage from "../../pages/objects-page";
import ObjectData from "./ObjectData";

const fakeObjects = [
  {
    id: 5,
    licensePlateNumber: "AA-BB-01",
    length: 4.6,
    bikeCarrier: true,
    type: "Caravan",
    secondaryOwners: [
      {
        id: 1,
        firstName: "Sjaakie",
        lastName: "Beveraal",
        street: "Vlakpad",
        houseNumber: "10",
        postalCode: "2222 BB",
        city: "Grenzeloos",
        phoneNumber: "06-12345678",
        email: "p.beveraal@kpnmail.nl",
        documents: [
          {
            id: 1,
            customersId: undefined,
            itemId: 5,
            uploadDate: "00-00-0000",
            size: "0kb",
            fileName: "filename.pdf",
            fileString: "",
          },
        ],
        notes: {
          id: 1,
          note: "test note",
          customerId: undefined,
          itemId: 5,
        },
      },
    ],
    owner: {
      id: 2,
      firstName: "Karel",
      lastName: "Agterdam",
      street: "Straatnaam",
      houseNumber: "1",
      postalCode: "1111 AA",
      city: "Meerdijk",
      phoneNumber: "010-1234567",
      email: "k.agterdam@gmail.com",
      documents: [
        {
          id: 2,
          customersId: 2,
          itemId: undefined,
          uploadDate: "00-00-0000",
          size: "0kb",
          fileName: "filename.pdf",
          fileString: "",
        },
      ],
      notes: {
        id: 2,
        note: "test note",
        customerId: 2,
        itemId: undefined,
      },
    },
    documents: [
      {
        id: 1,
        customersId: undefined,
        itemId: 5,
        uploadDate: "00-00-0000",
        size: "0kb",
        fileName: "filename.pdf",
        fileString: "",
      },
    ],
    notes: {
      id: 1,
      note: "test note",
      customerId: undefined,
      itemId: 5,
    },
  },
];

const afterSaveObjects = [
  {
    id: 5,
    licensePlateNumber: "XX-XX-99",
    length: 4.6,
    bikeCarrier: true,
    type: "Caravan",
    secondaryOwners: [
      {
        id: 1,
        firstName: "Sjaakie",
        lastName: "Beveraal",
        street: "Vlakpad",
        houseNumber: "10",
        postalCode: "2222 BB",
        city: "Grenzeloos",
        phoneNumber: "06-12345678",
        email: "p.beveraal@kpnmail.nl",
        documents: [
          {
            id: 1,
            customersId: undefined,
            itemId: 5,
            uploadDate: "00-00-0000",
            size: "0kb",
            fileName: "filename.pdf",
            fileString: "",
          },
        ],
        notes: {
          id: 1,
          note: "test note",
          customerId: undefined,
          itemId: 5,
        },
      },
    ],
    owner: {
      id: 2,
      firstName: "Karel",
      lastName: "Agterdam",
      street: "Straatnaam",
      houseNumber: "1",
      postalCode: "1111 AA",
      city: "Meerdijk",
      phoneNumber: "010-1234567",
      email: "k.agterdam@gmail.com",
      documents: [
        {
          id: 2,
          customersId: 2,
          itemId: undefined,
          uploadDate: "00-00-0000",
          size: "0kb",
          fileName: "filename.pdf",
          fileString: "",
        },
      ],
      notes: {
        id: 2,
        note: "test note",
        customerId: 2,
        itemId: undefined,
      },
    },
    documents: [
      {
        id: 1,
        customersId: undefined,
        itemId: 5,
        uploadDate: "00-00-0000",
        size: "0kb",
        fileName: "filename.pdf",
        fileString: "",
      },
    ],
    notes: {
      id: 1,
      note: "test note",
      customerId: undefined,
      itemId: 5,
    },
  },
];

describe("ObjectsPage", () => {
  let originalFetch: any;

  beforeEach(() => {
    originalFetch = global.fetch;
    jest
      .spyOn(global, "fetch")
      .mockImplementation(
        jest.fn(() =>
          Promise.resolve({ json: () => Promise.resolve(fakeObjects) })
        ) as jest.Mock
      );
  });

  afterEach(() => {
    global.fetch = originalFetch;
  });

  it("renders objects component", async () => {
    await act(async () => {
      render(
        <BrowserRouter>
          <ObjectsPage />
        </BrowserRouter>
      );
    });

    expect(screen.getByPlaceholderText("Search...")).toBeInTheDocument();
    expect(screen.getByText("Alle objecten")).toBeInTheDocument();
  });
});

describe("ObjectDetails", () => {
  let originalFetch: any;

  beforeEach(() => {
    originalFetch = global.fetch;
    jest
      .spyOn(global, "fetch")
      .mockImplementation(
        jest.fn(() =>
          Promise.resolve({ json: () => Promise.resolve(fakeObjects) })
        ) as jest.Mock
      );
  });

  afterEach(() => {
    global.fetch = originalFetch;
  });

  it("renders page with objectdata component", async () => {
    await act(async () => {
      render(
        <MemoryRouter initialEntries={["/5"]}>
          <ObjectsPage />
        </MemoryRouter>
      );
    });

    expect(screen.getByDisplayValue("AA-BB-01")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Caravan")).toBeInTheDocument();
    expect(screen.getByDisplayValue("4.6")).toBeInTheDocument();
  });
});

describe("ObjectData Component", () => {
  let originalFetch: any;

  beforeEach(() => {
    originalFetch = global.fetch;
    jest.spyOn(global, "fetch").mockImplementation(
      jest.fn(() =>
        Promise.resolve({
          json: () => Promise.resolve(afterSaveObjects),
        })
      ) as jest.Mock
    );
  });

  afterEach(() => {
    global.fetch = originalFetch;
  });

  it("renders and processes state change", async () => {
    await act(async () => {
      render(
        <BrowserRouter>
          <ObjectData object={fakeObjects[0]} />
        </BrowserRouter>
      );
    });

    const licensePlateNumberInput = screen.getByLabelText("Kenteken");
    expect(licensePlateNumberInput).toHaveValue("AA-BB-01");

    await act(async () => {
      fireEvent.change(licensePlateNumberInput, {
        target: { value: "XX-XX-99" },
      });
    });

    expect(licensePlateNumberInput).toHaveValue("XX-XX-99");
  });

  it("can submit form and save change", async () => {
    await act(async () => {
      render(
        <MemoryRouter initialEntries={["/5"]}>
          <ObjectsPage />
        </MemoryRouter>
      );
    });

    const licensePlateNumberInput = screen.getByLabelText("Kenteken");

    await act(async () => {
      fireEvent.change(licensePlateNumberInput, {
        target: { value: "XX-XX-99" },
      });
      fireEvent.submit(screen.getByTestId("opslaanObjectKnop"));
    });

    expect(licensePlateNumberInput).toHaveValue("XX-XX-99");
  });

  it("displays delete confirmation dialog", async () => {
    await act(async () => {
      render(
        <BrowserRouter>
          <ObjectData object={fakeObjects[0]} />
        </BrowserRouter>
      );
    });

    const deleteButton = screen.getByRole("button", {
      name: "deleteObjectBtn",
    });

    await act(async () => {
      fireEvent.click(deleteButton);
    });

    expect(screen.getByTestId("dialog-message")).toHaveTextContent(
      "Weet u zeker dat u dit object wilt verwijderen?"
    );
  });
});
