import { IObject } from "../../hooks/Models";
import { StringConstants } from "../../StringConstants";

export type ObjectValidationFields = {
    length: string,
    type: string,
    owner: string,
}

export const ValidateObjectFormFields = (fields: IObject) : { valid: boolean, errors: ObjectValidationFields} => {
    let errors: ObjectValidationFields = {
        length: "",
        type: "",
        owner: "",
      };

    let formIsValid = true;
    if (fields.length <= 0) {
      errors.length = StringConstants.LENGTH_REQUIRED;
    }
    if (fields.owner === null || fields.owner.id === 0 || fields.owner.id === undefined) {
      errors.owner = StringConstants.OWNER_REQUIRED;
    }
    if (fields.type.length === 0) {
      errors.type = StringConstants.TYPE_REQUIRED;
    }
    for (let key in errors) {
      if (errors[key as keyof ObjectValidationFields].length > 0) {
        formIsValid = false;
        break;
      }
    }

    return {
        valid: formIsValid,
        errors: errors
    }
}