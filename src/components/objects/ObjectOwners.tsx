import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { DataGrid, GridSelectionModel } from "@mui/x-data-grid";
import { useContext, useEffect, useMemo, useState } from "react";
import { MdDelete, MdEdit } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { ICustomer, IObject } from "../../hooks/Models";
import { fetchAllObjects, UpdateObject } from "../../hooks/useObjects";
import { StringConstants } from "../../StringConstants";
import { CeToastSettings } from "../shared/Toasts";
import CustomerTypeAhead from "../customers/CustomerTypeAhead";
import CustomPagination from "../shared/CustomPagination";
import DatagridUtilityBar from "../shared/DatagridUtilityBar";
import ObjectsContext from "../../context/ObjectsContext";

const ObjectOwners = (props: { object: IObject }) => {
  const navigate = useNavigate();
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openAddCustomer, setOpenAddCustomer] = useState(false);
  const [addCustomer, setAddCustomer] = useState<ICustomer>();
  const { updateObjects } = useContext(ObjectsContext);

  const linkedCustomers = useMemo(() => {
    const linked = [] as ICustomer[];

    linked.push(props.object.owner);
    props.object.secondaryOwners?.forEach((element) => {
      linked.push(element);
    });

    return linked;
  }, [props.object]);

  const [linkedCustomersRows, setlinkedCustomersRows] =
    useState<ICustomer[]>(linkedCustomers);

  const handleOnClickUpdateCustomers = () => {
    setOpenAddCustomer(true);
  };

  const handleCloseAddCustomer = () => {
    setOpenAddCustomer(false);
  };

  const submitUpdate = (newObject: IObject) => {
    UpdateObject(newObject)
        .then((response) => {
          fetchAllObjects().then((objects) => updateObjects(objects));
          toast.success(
            `${response.type} ${StringConstants.SUBMIT_SUCCEEDED}`,
            CeToastSettings
          );
        })
        .catch((error) => {
          toast.error(`${StringConstants.SUBMIT_ERROR}`, CeToastSettings);
          console.error(error);
        });
  }

  const handleConfirmAddCustomer = () => {
    setOpenAddCustomer(false);
    let newObject = props.object;
    if (addCustomer) {
      newObject.secondaryOwners?.push(addCustomer);
      submitUpdate(newObject);
    }
  };

  const handleChangeCustomerSelect = (value: ICustomer | null) => {
    if (value !== null) {
      setAddCustomer(value);
    }
  };

  useEffect(() => {
    setlinkedCustomersRows(linkedCustomers);
  }, [linkedCustomers]);

  function isPrimaryContact(params: any) {
    return params.id === params.api.state.rows.ids[0];
  }

  const handleOnClickObjectsDataGridRow = (param: any) => {
    navigate("/customers/" + param.id);
  };

  const handleDeleteSelection = () => {
    setOpenConfirmDialog(true);
  };

  const handleConfirmDelete = () => {
    setOpenConfirmDialog(false);
    let newObject = props.object;
    selectionModel.forEach((value) => {
      if (newObject.owner.id === value) {
        toast.error(`${StringConstants.DELETE_OWNER_ERROR}`, CeToastSettings);
      } else {
        newObject.secondaryOwners = newObject.secondaryOwners?.filter(
          (o) => o.id !== value
        );
      }
    });

    if (newObject.secondaryOwners?.length !== linkedCustomers.length - 1) {
      submitUpdate(newObject);
    }
  };

  const handleFilteredRows = (filteredSecondaryOwners: ICustomer[]) => {
    setlinkedCustomersRows(filteredSecondaryOwners);
  };

  const toggleEditMode = (e: any) => {
    e.target.blur();
    setEditMode(!editMode);
    setSelectionModel([]);
  };

  const handleCloseConfirmDialog = () => {
    setOpenConfirmDialog(false);
  };

  const objectData = {
    columns: [
      {
        field: "lastName",
        headerName: "Klantnaam",
        flex: 1,
        minWidth: 150,
      },
      {
        field: "firstName",
        headerName: "Voornaam",
        flex: 1,
        minWidth: 130,
      },
      {
        field: "email",
        headerName: "Email",
        flex: 1.4,
        minWidth: 190,
      },
      {
        field: "phoneNumber",
        headerName: "Telefoonnummer",
        flex: 1.3,
        minWidth: 130,
      },
      {
        field: "dummy",
        headerName: "Primair?",
        type: "boolean",
        minWidth: 140,
        valueGetter: isPrimaryContact,
      },
    ],
    rows: linkedCustomersRows === undefined ? [] : linkedCustomersRows,
  };

  const buttons = [
    {
      title: "Extra contactpersoon koppelen",
      onClick: handleOnClickUpdateCustomers,
      buttonClasses: "btn-primary",
    },
    {
      icon: <MdDelete />,
      onClick: handleDeleteSelection,
      buttonClasses: "btn-danger",
      disabled: !(editMode && selectionModel.length > 0),
      hide: !editMode,
    },
    {
      icon: <MdEdit />,
      onClick: toggleEditMode,
      buttonClasses: "btn-warning",
    },
  ];

  return (
    <>
      <DatagridUtilityBar
        title="Gekoppelde personen"
        data={linkedCustomers}
        setFilteredRows={handleFilteredRows}
        buttons={buttons}
      />
      <div className="data-grid detail-page-grid">
        <DataGrid
          {...objectData}
          disableSelectionOnClick
          disableColumnMenu
          autoPageSize
          rowHeight={44}
          checkboxSelection={editMode}
          onSelectionModelChange={(newSelectionModel) => {
            setSelectionModel(newSelectionModel);
          }}
          selectionModel={selectionModel}
          onRowClick={(params) => handleOnClickObjectsDataGridRow(params)}
          components={{
            Pagination: CustomPagination,
          }}
        />
      </div>
      <Dialog open={openAddCustomer} onClose={handleCloseAddCustomer}>
        <DialogTitle>Klant koppelen</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Zoek een klant om te koppelen aan dit object
          </DialogContentText>
          <CustomerTypeAhead onCustomerChange={handleChangeCustomerSelect} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseAddCustomer}>Cancel</Button>
          <Button variant="contained" onClick={handleConfirmAddCustomer}>
            Toevoegen
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openConfirmDialog} onClose={handleCloseConfirmDialog}>
        <DialogTitle>Ontkoppelen</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Weet u zeker dat u deze klant
            {selectionModel.length === 1 ? "" : "en"} wilt loskoppelen van dit
            object?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmDialog}>Cancel</Button>
          <Button
            variant="contained"
            color="error"
            onClick={handleConfirmDelete}
          >
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ObjectOwners;
