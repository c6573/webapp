import { DataGrid } from "@mui/x-data-grid";
import { useContext, useEffect, useState } from "react";
import CustomPagination from "../shared/CustomPagination";
import DatagridUtilityBar from "../shared/DatagridUtilityBar";
import { SiMicrosoftexcel } from "react-icons/si";
import ObjectsContext from "../../context/ObjectsContext";
import { IObject } from "../../hooks/Models";
import { useNavigate } from "react-router-dom";
import * as XLSX from "xlsx";

const Objects = () => {
  const { objects } = useContext(ObjectsContext);
  const [rows, setRows] = useState<IObject[]>(objects);
  const navigate = useNavigate();

  useEffect(() => {
    setRows(objects);
  }, [objects]);

  const handleOnClickDataGridRow = (param: any) => {
    navigate("/objects/" + param.id);
  };

  const handleFilteredRows = (filteredObjects: IObject[]) => {
    setRows(filteredObjects);
  };

  const handleOnClickNewObject = () => {
    navigate("/objects/add");
  };

  const handleOnClickExportButton = () => {
    const exportData = rows.map((r) => {
      const { owner, secondaryOwners, ...rest } = r;

      return {
        owner: `${owner.lastName} ${owner.firstName}`,
        ...rest,
      };
    });
    const ws = XLSX.utils.json_to_sheet(exportData);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "objects");
    XLSX.writeFile(wb, "objects.xlsx");
  };

  function getOwner(params: any) {
    return `${params.row.owner.lastName || ""}`;
  }

  function getOwnerFirstName(params: any) {
    return `${params.row.owner.firstName || ""}`;
  }

  const data = {
    columns: [
      {
        field: "owner.lastName",
        headerName: "Eigenaar",
        flex: 0.8,
        minWidth: 130,
        valueGetter: getOwner,
      },
      {
        field: "owner.firstName",
        headerName: "Voornaam eigenaar",
        flex: 0.7,
        minWidth: 100,
        valueGetter: getOwnerFirstName,
      },
      { field: "type", headerName: "Type", flex: 0.8, minWidth: 150 },
      {
        field: "length",
        headerName: "Lengte",
        flex: 0.7,
      },
      {
        field: "licensePlateNumber",
        headerName: "Kenteken",
        flex: 0.8,
        minWidth: 130,
      },
      {
        field: "bikeCarrier",
        headerName: "Fietsendrager",
        type: "boolean",
        minWidth: 140,
      },
    ],
    rows: rows,
  };

  const buttons = [
    {
      title: "Nieuw object",
      onClick: handleOnClickNewObject,
      buttonClasses: "btn-primary",
    },
    {
      icon: <SiMicrosoftexcel />,
      onClick: handleOnClickExportButton,
      buttonClasses: "btn-dark excel-icon",
    },
  ];

  return (
    <>
      <DatagridUtilityBar
        title="Alle objecten"
        data={objects}
        setFilteredRows={handleFilteredRows}
        buttons={buttons}
      />
      <div className="col-md-12 data-grid overview-page-grid">
        <DataGrid
          {...data}
          disableSelectionOnClick
          disableColumnMenu
          autoPageSize
          rowHeight={44}
          onRowClick={(params) => handleOnClickDataGridRow(params)}
          components={{
            Pagination: CustomPagination,
          }}
        />
      </div>
    </>
  );
};

export default Objects;
