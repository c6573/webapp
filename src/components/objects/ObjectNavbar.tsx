import { useParams } from "react-router-dom";
import { useContext } from "react";
import DetailViewNavBar from "../shared/DetailViewNavBar";
import ObjectsContext from "../../context/ObjectsContext";
import NoMatch from "../../pages/main-page/no-match";

const ObjectNavbar = () => {
  const { objects } = useContext(ObjectsContext);
  const { id } = useParams();

  let currentObject = objects.find((c) => c.id?.toString() === id);

  if (currentObject === undefined) {
    return <NoMatch />;
  }

  let currentIndex = objects.findIndex((c) => c.id?.toString() === id);
  let previousObject =
    currentIndex === 0 ? undefined : objects[currentIndex - 1];
  let nextObject =
    currentIndex === objects.length - 1 ? undefined : objects[currentIndex + 1];
  let name =
    currentObject?.owner.firstName + " " + currentObject?.owner.lastName;

  return (
    <DetailViewNavBar
      route="objects"
      previous={previousObject?.id}
      current={name}
      next={nextObject?.id}
    />
  );
};

export default ObjectNavbar;
