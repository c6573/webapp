import userEvent from "@testing-library/user-event";
import { render, screen, within } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import AddNewObject from "./AddNewObject";
import { StringConstants } from "../../StringConstants";

let typeSelectButton: any;
let lengthTextBox: any;

const testValues = {
  type: "De Grote",
  length: 4,
};

const setup = () => {
  render(
    <BrowserRouter>
      <AddNewObject />
    </BrowserRouter>
  );

  typeSelectButton = screen.getByRole("button", { name: /open/i });
  lengthTextBox = screen.getByRole("spinbutton", { name: /lengte/i });
};
describe("New object form", () => {
  beforeEach(() => {});
  it("renders", () => {
    setup();
    expect(screen.getByText("Toevoegen nieuw object")).toBeInTheDocument();
  });
  it("should be empty after load", () => {
    setup();
    expect(
      screen.getByRole("form", {
        name: /add new object/i,
      })
    ).toHaveFormValues({
      type: "",
      length: 0,
      licensePlateNumber: "",
      bikeCarrier: false,
    });
  });
  it("should accept input", async () => {
    setup();
    const user = userEvent.setup();
    await user.type(lengthTextBox, testValues.length.toString());

    expect(
      screen.getByRole("form", {
        name: /add new object/i,
      })
    ).toHaveFormValues({
      type: "",
      length: testValues.length,
      licensePlateNumber: "",
      bikeCarrier: false,
    });
  });
  it("should validate properly and have an error on type", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("type-error"));
    expect(getByText(StringConstants.TYPE_REQUIRED)).toBeInTheDocument();
  });
  it("should validate properly and have an error on length", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("length-error"));
    expect(getByText(StringConstants.LENGTH_REQUIRED)).toBeInTheDocument();
  });
  it("should validate properly and have an error on owner", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("owner-error"));
    expect(getByText(StringConstants.OWNER_REQUIRED)).toBeInTheDocument();
  });
});
