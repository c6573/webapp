import { ICustomer, IObject } from "../../hooks/Models";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  MenuItem,
  Select,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import CustomerTypeAhead from "../customers/CustomerTypeAhead";
import {
  DeleteObject,
  fetchAllObjects,
  UpdateObject,
} from "../../hooks/useObjects";
import { toast } from "react-toastify";
import { CeToastSettings } from "../shared/Toasts";
import { StringConstants } from "../../StringConstants";
import ObjectsContext from "../../context/ObjectsContext";
import { MdDelete } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import {
  ValidateObjectFormFields,
  ObjectValidationFields,
} from "./ObjectFormValidation";

const ObjectData = (props: { object: IObject }) => {
  const { updateObjects } = useContext(ObjectsContext);
  const objectTypes = process.env.REACT_APP_OBJECTTYPES?.split(",") ?? [];
  const [selectedCustomer, setSelectedCustomer] = useState(undefined);
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const navigate = useNavigate();
  let typeOptions: any = [];

  objectTypes.forEach((element, index) =>
    typeOptions.push(
      <MenuItem key={index} value={element}>
        {element}
      </MenuItem>
    )
  );

  const errors: ObjectValidationFields = {
    length: "",
    type: "",
    owner: "",
  };

  const [state, setState] = useState({
    fields: { ...props.object },
    errors: errors,
    isFormDirty: false,
  });

  useEffect(() => {
    setState({
      fields: { ...props.object },
      errors: errors,
      isFormDirty: false,
    });
  }, [props.object]);

  const handleChange = (e: any) => {
    const name = e.target.name;
    const value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    setState({
      ...state,
      fields: {
        ...state.fields,
        [name]: value,
      },
      isFormDirty: true,
    });
  };

  const handleValidation = () => {
    const validation = ValidateObjectFormFields(state.fields);
    setState({
      ...state,
      errors: validation.errors,
    });
    return validation.valid;
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    let newObject = state.fields;
    if (selectedCustomer !== undefined) {
      if (
        props.object.secondaryOwners?.some(
          (s) => s.id === (selectedCustomer as ICustomer).id
        )
      ) {
        toast.error(`${StringConstants.CHANGE_OWNER_ERROR}`, CeToastSettings);
        return;
      } else {
        newObject.owner = selectedCustomer;
      }
    }
    newObject.length = Number(state.fields.length);
    if (handleValidation()) {
      UpdateObject(newObject)
        .then((response) => {
          fetchAllObjects().then((objects) => updateObjects(objects));
          toast.success(
            `${response.type} ${StringConstants.SUBMIT_SUCCEEDED}`,
            CeToastSettings
          );
        })
        .catch((error) => {
          toast.error(`${StringConstants.SUBMIT_ERROR}`, CeToastSettings);
          console.error(error);
        });
    }
  };

  const handleOnClickDeleteButton = (e: any) => {
    e.target.blur();
    setOpenConfirmDialog(true);
  };

  const handleCloseConfirmDialog = () => {
    setOpenConfirmDialog(false);
  };

  const handleConfirmDelete = () => {
    setOpenConfirmDialog(false);
    if (state.fields.id) {
      DeleteObject(state.fields.id.toString())
        .then(() => {
          fetchAllObjects().then((objects) => updateObjects(objects));
          toast.success(
            `${state.fields.type} ${StringConstants.DELETE_SUCCEEDED}`,
            CeToastSettings
          );
          navigate("/objects");
        })
        .catch((error) => {
          toast.error(error, CeToastSettings);
        });
    }
  };

  const handleChangeCustomerSelect = (value: any) => {
    setSelectedCustomer(value);
    setState({
      ...state,
      isFormDirty: true,
    });
  };

  return (
    <div key={props.object.id}>
      <form id="objectData" onSubmit={handleSubmit}>
        <div className="row mt-3 mb-3">
          <div className="col-md-6">
            <h3>Objectgegevens</h3>
          </div>
          <div className="col-md-6 d-inline-flex justify-content-end">
            <button
              data-testid="opslaanObjectKnop"
              className="btn btn-primary btn-sm me-1"
              type="submit"
              disabled={!state.isFormDirty}
            >
              Opslaan
            </button>
            <button
              id="deleteObjectBtn"
              aria-label="deleteObjectBtn"
              className="btn btn-danger me-1"
              type="button"
              onClick={handleOnClickDeleteButton}
            >
              <MdDelete />
            </button>
          </div>
        </div>
        <div className="row mb-2">
          <label htmlFor="type" className="col-md-3 col-xl-2 col-form-label">
            Type
          </label>
          <div className="col-md-3 col-lg-3">
            <FormControl fullWidth>
              <Select
                id="type"
                name="type"
                onChange={handleChange}
                value={state.fields.type}
              >
                {typeOptions}
              </Select>
            </FormControl>
            <div className="invalid-feedback">{state.errors.type}</div>
          </div>
          <label
            htmlFor="kenteken"
            className="col-md-3 col-xl-2 col-form-label offset-xl-1"
          >
            Kenteken
          </label>
          <div className="col-md-3 col-lg-3">
            <input
              type="text"
              className="form-control"
              name="licensePlateNumber"
              onChange={handleChange}
              id="kenteken"
              value={
                state.fields.licensePlateNumber === null
                  ? ""
                  : state.fields.licensePlateNumber
              }
            />
          </div>
        </div>
        <div className="row mb-2">
          <label
            htmlFor="contactpersoon"
            className="col-md-3 col-xl-2 col-form-label"
          >
            Primair contactpersoon
          </label>
          <div className="col-md-3 col-lg-3">
            <CustomerTypeAhead
              customer={state.fields.owner}
              onCustomerChange={handleChangeCustomerSelect}
            />
            <div className="invalid-feedback">{state.errors.owner}</div>
          </div>
          <label
            htmlFor="length"
            className="col-md-3 col-xl-2 col-form-label offset-xl-1"
          >
            Lengte (m)
          </label>
          <div className="col-md-3 col-lg-3">
            <input
              type="number"
              className="form-control"
              id="length"
              name="length"
              onChange={handleChange}
              value={state.fields.length}
            />
            <div className="invalid-feedback">{state.errors.length}</div>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-md-6 mt-1 form-check">
            <input
              type="checkbox"
              className="form-check-input ce-checkbox"
              id="bikeCarrier"
              name="bikeCarrier"
              onChange={handleChange}
              defaultChecked={state.fields.bikeCarrier}
            />
            <label htmlFor="bikeCarrier" className="form-check-label">
              Fietsendrager
            </label>
          </div>
        </div>
      </form>
      <Dialog open={openConfirmDialog} onClose={handleCloseConfirmDialog}>
        <DialogTitle>Verwijderen</DialogTitle>
        <DialogContent>
          <DialogContentText data-testid="dialog-message">
            Weet u zeker dat u dit object wilt verwijderen?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmDialog}>Cancel</Button>
          <Button
            variant="contained"
            color="error"
            onClick={handleConfirmDelete}
          >
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ObjectData;
