import {
  Autocomplete,
  CircularProgress,
  FormControl,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import ObjectsContext from "../../context/ObjectsContext";
import { ICustomer } from "../../hooks/Models";
import { fetchAllCustomers } from "../../hooks/useCustomers";
import { AddObject, fetchAllObjects } from "../../hooks/useObjects";
import { StringConstants } from "../../StringConstants";
import { CeToastSettings } from "../shared/Toasts";
import {
  ValidateObjectFormFields,
  ObjectValidationFields,
} from "./ObjectFormValidation";

const AddNewObject = () => {
  const { updateObjects } = useContext(ObjectsContext);
  const objectTypes = process.env.REACT_APP_OBJECTTYPES?.split(",") ?? [];
  let typeOptions: any = [];
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState<readonly ICustomer[]>([]);
  const [owner, setOwner] = useState<ICustomer | null>(null);
  const loading = open && options.length === 0;

  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  useEffect(() => {
    let active = true;

    if (!loading) {
      return undefined;
    }

    (async () => {
      const allCustomers = await fetchAllCustomers();

      if (active) {
        setOptions(allCustomers);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading]);

  objectTypes.forEach((element, index) =>
    typeOptions.push(
      <MenuItem key={index} value={element}>
        {element}
      </MenuItem>
    )
  );
  const object: any = {
    licensePlateNumber: "",
    length: 0,
    type: "",
    bikeCarrier: false,
    owner: { id: 0 },
  };

  const errors: ObjectValidationFields = {
    length: "",
    type: "",
    owner: "",
  };

  const [state, setState] = useState({
    fields: { ...object },
    errors: { ...errors },
  });

  const handleChange = (e: any) => {
    const name = e.target.name;
    let val = e.target.type === "checkbox" ? e.target.checked : e.target.value;
    if (e.target.type === "number") val = Number(e.target.value);

    setState({
      ...state,
      fields: {
        ...state.fields,
        [name]: val,
      },
    });
  };

  const handleResetForm = (e: any) => {
    e.preventDefault();
    setState({
      fields: { ...object },
      errors: { ...errors },
    });
    setOwner(null);
  };

  const handleValidation = () => {
    let data = state.fields;
    data.owner.id = owner?.id;
    const validation = ValidateObjectFormFields(data);
    setState({
      ...state,
      errors: validation.errors,
    });
    return validation.valid;
  };

  const saveNewCustomer = (e: any) => {
    e.preventDefault();
    if (handleValidation()) {
      let data = state.fields;
      data.owner.id = owner?.id;
      AddObject(data)
        .then((response) => {
          fetchAllObjects().then((objects) => updateObjects(objects));
          toast.success(
            `${response.type} ${StringConstants.SUBMIT_SUCCEEDED}`,
            CeToastSettings
          );
          handleResetForm(e);
        })
        .catch((error) => {
          toast.error(`${StringConstants.SUBMIT_ERROR}`, CeToastSettings);
          console.error(error);
        });
    }
  };

  return (
    <>
      <h3 className="mt-3 mb-3">Toevoegen nieuw object</h3>
      <form
        name="newCustomerForm"
        onSubmit={saveNewCustomer}
        aria-label="Add new object"
      >
        <div className="row mb-3">
          <div className="col-lg-12">
            <fieldset>
              <div className="row mb-2">
                <label htmlFor="type" className="col-form-label col-md-3">
                  Type
                </label>
                <div className="col-md-9">
                  <FormControl fullWidth>
                    <Select
                      id="type"
                      name="type"
                      value={state.fields.type}
                      onChange={handleChange}
                    >
                      {typeOptions}
                    </Select>
                  </FormControl>
                  <div className="invalid-feedback" data-testid="type-error">
                    {state.errors.type}
                  </div>
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="length" className="col-form-label col-md-3">
                  Lengte
                </label>
                <div className="col-md-9">
                  <input
                    type="number"
                    name="length"
                    className="form-control"
                    id="length"
                    value={state.fields.length}
                    onChange={handleChange}
                  />
                  <div className="invalid-feedback" data-testid="length-error">
                    {state.errors.length}
                  </div>
                </div>
              </div>
              <div className="row mb-2">
                <label
                  htmlFor="licensePlateNumber"
                  className="col-form-label col-md-3"
                >
                  Kenteken
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="licensePlateNumber"
                    className="form-control"
                    id="licensePlateNumber"
                    value={state.fields.licensePlateNumber}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="owner" className="col-form-label col-md-3">
                  Eigenaar
                </label>
                <div className="col-md-9">
                  <Autocomplete
                    id="owner"
                    options={options}
                    value={owner}
                    onChange={(_event: any, newValue: ICustomer | null) => {
                      setOwner(newValue);
                    }}
                    getOptionLabel={(option: ICustomer) =>
                      `${option.firstName} ${option.lastName}`
                    }
                    isOptionEqualToValue={(opt, val) => opt.id === val.id}
                    open={open}
                    onOpen={() => {
                      setOpen(true);
                    }}
                    onClose={() => {
                      setOpen(false);
                    }}
                    loading={loading}
                    size="small"
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {loading ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                      />
                    )}
                  />
                  <div className="invalid-feedback" data-testid="owner-error">
                    {state.errors.owner}
                  </div>
                </div>
              </div>
              <div className="row mb-2">
                <label
                  htmlFor="bikeCarrier"
                  className="col-form-label col-md-3"
                >
                  Fietsendrager aanwezig?
                </label>
                <div className="col-md-9">
                  <input
                    type="checkbox"
                    className="form-check-input ce-checkbox"
                    id="bikeCarrier"
                    name="bikeCarrier"
                    checked={state.fields.bikeCarrier}
                    onChange={handleChange}
                  />
                </div>
              </div>
            </fieldset>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col d-inline-flex justify-content-end">
            <button
              className="btn btn-secondary me-1"
              onClick={handleResetForm}
            >
              Reset
            </button>
            <button className="btn btn-primary me-1" type="submit">
              Opslaan
            </button>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddNewObject;
