import { useContext } from "react";
import { useParams } from "react-router-dom";
import ObjectsContext from "../../context/ObjectsContext";
import ObjectData from "./ObjectData";
import ObjectOwners from "./ObjectOwners";
import DocumentUpload from "../document-upload";

const ObjectDetailView = () => {
  const { id } = useParams();
  const { objects } = useContext(ObjectsContext);

  return (
    <div className="row mt-3">
      {objects
        .filter((c) => c.id?.toString() === id)
        .map((object, index) => (
          <div key={index} className="col customerdetails">
            <ul className="nav nav-pills mb-3 shadow-sm" id="myTab">
              <li className="nav-item">
                <a
                  href="#details"
                  className="nav-link active"
                  data-bs-toggle="tab"
                >
                  Details
                </a>
              </li>
              <li className="nav-item">
                <a href="#dossier" className="nav-link" data-bs-toggle="tab">
                  Dossier
                </a>
              </li>
            </ul>
            <div className="tab-content">
              <div className="tab-pane fade show active" id="details">
                <ObjectData object={object} />
                <ObjectOwners object={object} />
              </div>
              <div className="tab-pane fade" id="dossier">
                <DocumentUpload
                  itemId={object.id}
                  documents={object.documents}
                            notes={object.notes}
                            uploadType="object"
                ></DocumentUpload>
              </div>
            </div>
          </div>
        ))}
    </div>
  );
};

export default ObjectDetailView;
