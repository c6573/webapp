import { render, screen } from "@testing-library/react";
import Sidebar from ".";
import { BrowserRouter } from "react-router-dom";

let container: HTMLElement;
beforeEach(() => {
  container = render(
    <BrowserRouter>
      <Sidebar />
    </BrowserRouter>
  ).container;
});

const links = [
  { text: "Klanten", location: "/customers" },
  { text: "Objecten", location: "/objects" },
  { text: "Email template", location: "/email-template" },
];

describe("Sidebar", () => {
  it("renders the app title", () => {
    const logo = screen.getByText(/Caravanstalling Elst/);
    expect(logo).toBeInTheDocument();
  });
  it.each(links)("Check if sidebar has %s link.", (link) => {
    const linkDom = screen.getByText(link.text).parentNode;
    expect(linkDom).toHaveAttribute("href", link.location);
  });
});
