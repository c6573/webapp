import { NavLink, useNavigate } from "react-router-dom";
import logo from "./caravan.svg";
import {
  MdPeople,
  MdOutlineDirectionsCar,
  MdOutlineEmail,
} from "react-icons/md";
import "./sidebar.css";

const Sidebar = () => {
  const navigate = useNavigate();
  const onClick = () => {
    navigate(`/`);
  };

  return (
    <div
      id="sidebar"
      className="d-flex flex-column p-3 flex-shrink-0 min-vh-100"
    >
      <div
        id="nav-header"
        className="clickable"
        onClick={onClick}
        data-testid="caravanstalling-elst-logo"
      >
        <img
          src={logo}
          className="icon-sidebar"
          alt="Caravanstalling Elst Logo"
        />
        <span className="fs-5 align-middle">Caravanstalling Elst</span>
        <hr />
      </div>
      <ul className="nav nav-pills flex-column mb-auto">
        <li className="nav-item">
          <NavLink to="/customers" className="nav-link link-dark">
            <MdPeople className="nav-icon" />
            <span className="nav-text">Klanten</span>
          </NavLink>{" "}
        </li>
        <hr className="nav-seperator" />
        <li className="nav-item">
          <NavLink to="/objects" className="nav-link link-dark">
            <MdOutlineDirectionsCar className="nav-icon" />
            <span className="nav-text">Objecten</span>
          </NavLink>{" "}
        </li>
        <hr className="nav-seperator" />
        <li className="nav-item">
          <NavLink to="/email-template" className="nav-link link-dark">
            <MdOutlineEmail className="nav-icon" />
            <span className="nav-text">Email template</span>
          </NavLink>{" "}
        </li>
        <hr className="nav-seperator" />
      </ul>
    </div>
  );
};

export default Sidebar;
