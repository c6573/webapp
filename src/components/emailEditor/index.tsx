import { useRef, useState, useContext, useEffect } from "react";
import EmailEditor from "react-email-editor";
import { toast } from "react-toastify";
import EmailTemplateContext from "../../context/EmailTemplateContext";
import { IEmailTemplate } from "../../hooks/Models";
import "./emailTemplate.css";
import "react-toastify/dist/ReactToastify.css";
import { CeToastSettings } from "../shared/Toasts";
import { StringConstants } from "../../StringConstants";

const DynamicEmailEditor = () => {
  const emailEditorRef: any = useRef(null);
  const emailTemplatesContext = useContext(EmailTemplateContext);
  const [emailTemplates, setEmailTemplates] = useState<IEmailTemplate[]>(
    emailTemplatesContext
  );
  const [currentEmailTemplate, setCurrentEmailTemplate] =
    useState<IEmailTemplate>();
  const [subjectText, setSubjectText] = useState("");

  useEffect(() => {
    setEmailTemplates(emailTemplatesContext);
  }, [emailTemplatesContext]);

  useEffect(() => {
    getInitialState();
    onLoad();
  }, [emailTemplates]);

  const saveTemplate = () => {
    emailEditorRef.current?.editor.exportHtml((data: any) => {
      const { design, html } = data;

      const emailTemplate: IEmailTemplate = {
        id: currentEmailTemplate?.id,
        subject: subjectText,
        emailType: currentEmailTemplate?.emailType,
        htmlString: html,
        jsonTemplate: JSON.stringify(design),
      };

      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(emailTemplate),
      };
      fetch(
        process.env.REACT_APP_API_URL + "save-email-template",
        requestOptions
      )
        .then((response) => response.json())
        .then((templates) => {
          setCurrentEmailTemplate(emailTemplate);
          setEmailTemplates(templates);
          toast.success(
            `${emailTemplate.emailType} ${StringConstants.SUBMIT_SUCCEEDED}`,
            CeToastSettings
          );
        })
        .catch((error) => {
          toast.error(StringConstants.SUBMIT_ERROR, CeToastSettings);
          console.error(error);
        });
    });
  };

  const onLoad = () => {
    setTimeout(() => {
      if (emailTemplates !== undefined && emailTemplates.length > 0) {
        if (
          emailTemplates[0].jsonTemplate.length > 0 &&
          !currentEmailTemplate
        ) {
          const templateJson = JSON.parse(emailTemplates[0].jsonTemplate);
          emailEditorRef.current.editor.loadDesign(templateJson);
        } else if (currentEmailTemplate) {
          emailEditorRef.current.editor.loadDesign(
            JSON.parse(currentEmailTemplate.jsonTemplate)
          );
        } else {
          emailEditorRef.current.editor.loadDesign();
        }
      }
    }, 200);
  };

  const getInitialState = () => {
    if (emailTemplates && emailTemplates.length > 0 && !currentEmailTemplate) {
      const initialTemplate = emailTemplates[0].emailType;
      setCurrentEmailTemplate(emailTemplates[0]);
      setSubjectText(emailTemplates[0].subject);
      return initialTemplate;
    }
  };

  const [value, setValue] = useState(getInitialState);

  const handleChange = (e: any) => {
    setValue(e.target.value);
    emailTemplates.map((emailTemplate) => {
      if (emailTemplate.emailType === e.target.value) {
        if (emailTemplate.jsonTemplate.length > 0) {
          emailEditorRef.current.editor.loadDesign(
            JSON.parse(emailTemplate.jsonTemplate)
          );
          setCurrentEmailTemplate(emailTemplate);
          setSubjectText(emailTemplate.subject);
        } else {
            emailEditorRef.current.editor.loadBlank({
                backgroundColor: '#e7e7e7'
            });
          setCurrentEmailTemplate(emailTemplate);
          setSubjectText(emailTemplate.subject);
        }
      }
    });
  };

  let options = emailTemplates.map((emailTemplate) => {
    return (
      <option key={emailTemplate.id} value={emailTemplate.emailType}>
        {emailTemplate.emailType}
      </option>
    );
  });

  return (
    <div>
      <h2 className="mt-3">Email template editor</h2>
      <p>Selecteer hier het template dat je wilt wijzigen</p>
      <div className="flexbox-container">
        <div>
          <select value={value} onChange={handleChange}>
            {options}
          </select>
        </div>
        <div>
          <button className="btn btn-primary me-1" onClick={saveTemplate}>
            Opslaan
          </button>
        </div>
      </div>
      <div>
        <div className="row mb-2">
          <label
            htmlFor="emailTemplateSubject"
            className="col-form-label customer-form-label"
          >
            Onderwerp
          </label>
          <div className="col-md-6 col-lg-6">
            <input
              type="text"
              id="emailTemplateSubject"
              className="form-control"
              value={subjectText}
              onChange={(i) => setSubjectText(i.target.value)}
            />
          </div>
        </div>
      </div>
      <EmailEditor ref={emailEditorRef} onLoad={onLoad} minHeight={800} />
    </div>
  );
};

export default DynamicEmailEditor;
