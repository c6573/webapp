import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import CustomersContext from "../../context/CustomersContext";
import { ICustomer } from "../../hooks/Models";
import { fetchAllCustomers, UpdateCustomer } from "../../hooks/useCustomers";
import { StringConstants } from "../../StringConstants";
import { CeToastSettings } from "../shared/Toasts";
import {
  ValidateCustomerFormFields,
  CustomerValidationFields,
} from "./CustomerFormValidation";

const CustomerData = (props: { customer: ICustomer }) => {
  const { updateCustomers } = useContext(CustomersContext);
  const errors: CustomerValidationFields = {
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
  };

  const [state, setState] = useState({
    fields: { ...props.customer },
    errors: errors,
    isFormDirty: false,
  });

  useEffect(() => {
    setState({
      fields: { ...props.customer },
      errors: errors,
      isFormDirty: false,
    });
  }, [props.customer]);

  const handleChange = (e: any) => {
    setState({
      ...state,
      fields: {
        ...state.fields,
        [e.target.name]: e.target.value,
      },
      isFormDirty: true,
    });
  };

  const handleValidation = () => {
    const validation = ValidateCustomerFormFields(state.fields);
    setState({
      ...state,
      errors: validation.errors,
    });
    return validation.valid;
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (handleValidation()) {
      UpdateCustomer(state.fields)
        .then((response) => {
          fetchAllCustomers().then((customers) => updateCustomers(customers));
          toast.success(
            `${response.firstName} ${response.lastName} ${StringConstants.SUBMIT_SUCCEEDED}`,
            CeToastSettings
          );
        })
        .catch((error) => {
          toast.error(`${StringConstants.SUBMIT_ERROR}`, CeToastSettings);
          console.error(error);
        });
    }
  };

  return (
    <div key={props.customer.id}>
      <form id="customerDataForm" onSubmit={handleSubmit}>
        <div className="row mt-3 mb-3">
          <div className="col-md-6">
            <h3>Klantgegevens</h3>
          </div>
          <div className="col-md-6 d-inline-flex justify-content-end">
            <button
              className="btn btn-primary btn-sm me-1"
              data-testid="opslaanCustomerKnop"
              type="submit"
              disabled={!state.isFormDirty}
            >
              Opslaan
            </button>
          </div>
        </div>
        <div className="row mb-3 ">
          <div className="col-lg-12">
            <div className="row mb-2">
              <label
                htmlFor="customerName"
                className="col-form-label customer-form-label"
              >
                Achternaam
              </label>
              <div className="col-md-4 col-lg-4">
                <input
                  type="text"
                  className="form-control"
                  id="customerName"
                  name="lastName"
                  value={state.fields.lastName}
                  onChange={handleChange}
                />
                <div className="invalid-feedback" data-testid="lastname-error">
                  {state.errors.lastName}
                </div>
              </div>
              <label
                htmlFor="customerFirstName"
                className="col-form-label customer-form-label offset-xl-1"
              >
                Voornaam
              </label>
              <div className="col-md-3 col-lg-3">
                <input
                  type="text"
                  className="form-control"
                  id="customerFirstName"
                  name="firstName"
                  value={state.fields.firstName}
                  onChange={handleChange}
                />
                <div className="invalid-feedback">{state.errors.firstName}</div>
              </div>
            </div>
            <div className="row mb-2">
              <label
                htmlFor="customerStreet"
                className="col-form-label customer-form-label"
              >
                Straat
              </label>
              <div className="col-md-4 col-lg-4">
                <input
                  type="text"
                  className="form-control"
                  id="customerStreet"
                  name="street"
                  value={
                    state.fields.street === null ? "" : state.fields.street
                  }
                  onChange={handleChange}
                />
              </div>
              <label
                htmlFor="customerHouseNumber"
                className="col-form-label customer-form-label offset-xl-1"
              >
                Huisnummer
              </label>
              <div className="col-md-3 col-lg-3">
                <input
                  type="text"
                  className="form-control"
                  id="customerHouseNumber"
                  name="houseNumber"
                  value={
                    state.fields.houseNumber === null
                      ? ""
                      : state.fields.houseNumber
                  }
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="row mb-2">
              <label
                htmlFor="customerPostalCode"
                className="col-form-label customer-form-label"
              >
                Postcode
              </label>
              <div className="col-md-4 col-lg-4">
                <input
                  type="text"
                  className="form-control"
                  id="customerPostalCode"
                  name="postalCode"
                  value={
                    state.fields.postalCode === null
                      ? ""
                      : state.fields.postalCode
                  }
                  onChange={handleChange}
                />
              </div>
              <label
                htmlFor="customerCity"
                className="col-form-label customer-form-label offset-xl-1"
              >
                Plaats
              </label>
              <div className="col-md-3 col-lg-3">
                <input
                  type="text"
                  className="form-control"
                  id="customerCity"
                  name="city"
                  value={state.fields.city === null ? "" : state.fields.city}
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="row mb-2">
              <label
                htmlFor="customerEmail"
                className="col-form-label customer-form-label"
              >
                Email
              </label>
              <div className="col-md-6 col-lg-6">
                <input
                  type="email"
                  className="form-control"
                  id="customerEmail"
                  name="email"
                  value={state.fields.email}
                  onChange={handleChange}
                />
                <div className="invalid-feedback">{state.errors.email}</div>
              </div>
            </div>
            <div className="row mb-2">
              <label
                htmlFor="customerPhone"
                className="col-form-label customer-form-label"
              >
                Telefoonnummer
              </label>
              <div className="col-md-6 col-lg-6">
                <input
                  type="text"
                  className="form-control"
                  id="customerPhone"
                  name="phoneNumber"
                  minLength={10}
                  maxLength={20}
                  defaultValue={state.fields.phoneNumber}
                  onChange={handleChange}
                />
                <div className="invalid-feedback">
                  {state.errors.phoneNumber}
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default CustomerData;
