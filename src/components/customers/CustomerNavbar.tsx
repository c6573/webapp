import { useParams } from "react-router-dom";
import { useContext } from "react";
import CustomersContext from "../../context/CustomersContext";
import DetailViewNavBar from "../shared/DetailViewNavBar";
import NoMatch from "../../pages/main-page/no-match";

const CustomerNavbar = () => {
  const { customers } = useContext(CustomersContext);
  const { id } = useParams();

  let currentCustomer = customers.find((c) => c.id?.toString() === id);

  if (currentCustomer === undefined) {
    return <NoMatch />;
  }

  let currentIndex = customers.findIndex((c) => c.id?.toString() === id);
  let previousCustomer =
    currentIndex === 0 ? undefined : customers[currentIndex - 1];
  let nextCustomer =
    currentIndex === customers.length - 1
      ? undefined
      : customers[currentIndex + 1];
  let name = currentCustomer?.firstName + " " + currentCustomer?.lastName;

  return (
    <DetailViewNavBar
      route="customers"
      previous={previousCustomer?.id}
      current={name}
      next={nextCustomer?.id}
    />
  );
};

export default CustomerNavbar;
