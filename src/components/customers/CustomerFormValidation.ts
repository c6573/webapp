import { ICustomer } from "../../hooks/Models";
import { StringConstants } from "../../StringConstants";

const emailRegex: RegExp = /^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/i;
const phoneRegex: RegExp =
    /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?\d)((\s|\s?-\s?)?\d)((\s|\s?-\s?)?\d)\s?\d\s?\d\s?\d\s?\d\s?\d$/;

export type CustomerValidationFields = {
    firstName: string,
    lastName: string,
    phoneNumber: string,
    email: string,
}

export const ValidateCustomerFormFields = (fields: ICustomer) : { valid: boolean, errors: CustomerValidationFields} => {
    let errors: CustomerValidationFields = {
        firstName: "",
        lastName: "",
        phoneNumber: "",
        email: "",
      };

    let formIsValid = true;
    if (fields.firstName.length === 0) {
      errors.firstName = StringConstants.FIRSTNAME_REQUIRED;
    }
    if (fields.lastName.length === 0) {
      errors.lastName = StringConstants.LASTNAME_REQUIRED;
    }
    if (fields.phoneNumber.length === 0) {
      errors.phoneNumber = StringConstants.PHONENUMBER_REQUIRED;
    } else if (!fields.phoneNumber.match(phoneRegex)) {
      errors.phoneNumber = StringConstants.PHONENUMBER_REGEX;
    }
    if (fields.email.length === 0) {
      errors.email = StringConstants.EMAIL_REQUIRED;
    } else if (!fields.email.match(emailRegex)) {
      errors.email = StringConstants.EMAIL_REGEX;
    }

    for (let key in errors) {
      if (errors[key as keyof CustomerValidationFields].length > 0) {
        formIsValid = false;
        break;
      }
    }

    return {
        valid: formIsValid,
        errors: errors
    }
}