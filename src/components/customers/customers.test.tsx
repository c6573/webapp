import { act, fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter, MemoryRouter } from "react-router-dom";
import { ICustomer, IDocument } from "../../hooks/Models";
import CustomersPage from "../../pages/customers-page";
import { StringConstants } from "../../StringConstants";
import DetailViewNavBar from "../shared/DetailViewNavBar";
import AddNewCustomer from "./AddNewCustomer";
import CustomerData from "./CustomerData";
import CustomerNavbar from "./CustomerNavbar";

const fakeCustomers = [
  {
    id: 10,
    firstName: "Wiley",
    lastName: "Bowers",
    street: "Johnson Street",
    houseNumber: "33",
    postalCode: "4298 WY",
    city: "Iberia",
    email: "bowerswiley@zentix.com",
    phoneNumber: "(826) 449-3113",
    documents: [] as IDocument[],
    notes: undefined,
    items: [
      {
        id: 1,
        licensePlateNumber: "QL-MP-17",
        length: 4.9,
        type: "Vouwwagen",
        bikeCarrier: false,
        owner: undefined,
        notes: undefined,
        secondaryOwners: [] as ICustomer[],
        documents: [] as IDocument[],
      },
      {
        id: 2,
        licensePlateNumber: "",
        length: 4.7,
        type: "Camper",
        bikeCarrier: true,
        owner: undefined,
        notes: undefined,
        secondaryOwners: [] as ICustomer[],
        documents: [] as IDocument[],
      },
    ],
  },
];

describe("DetailViewNavbar", () => {
  it("navbar shows title and previous and next links", () => {
    render(
      <BrowserRouter>
        <DetailViewNavBar
          route="customers"
          previous={1}
          next={3}
          current="titel"
        />
      </BrowserRouter>
    );

    expect(document.getElementsByClassName("navbar-text")[0].innerHTML).toBe(
      "titel"
    );
    const links = document.getElementsByTagName("a");
    expect(links[0].href).toContain("1");
    expect(links[1].href).toContain("3");
  });
  it("navbar shows an title, active next link and a disabled previous button", () => {
    render(
      <BrowserRouter>
        <DetailViewNavBar
          route="customers"
          previous={undefined}
          next={2}
          current="titel"
        />
      </BrowserRouter>
    );

    expect(document.getElementsByClassName("navbar-text")[0].innerHTML).toBe(
      "titel"
    );
    const links = document.getElementsByTagName("a");
    const buttons = document.getElementsByTagName("button");
    expect(links[0].href).toContain("2");
    expect(buttons[0]).toHaveClass("disabled");
  });
  it("navbar shows title, disabled next button and a previous link", () => {
    render(
      <BrowserRouter>
        <DetailViewNavBar
          route="customers"
          previous={3}
          next={undefined}
          current="titel"
        />
      </BrowserRouter>
    );

    expect(document.getElementsByClassName("navbar-text")[0].innerHTML).toBe(
      "titel"
    );
    const links = document.getElementsByTagName("a");
    const buttons = document.getElementsByTagName("button");
    expect(links[0].href).toContain("3");
    expect(buttons[0]).toHaveClass("disabled");
  });
});

describe("CustomerNavbar", () => {
  it("renders", () => {
    render(
      <BrowserRouter>
        <CustomerNavbar />
      </BrowserRouter>
    );
  });
});

describe("CustomerPage", () => {
  let originalFetch: any;

  beforeEach(() => {
    originalFetch = global.fetch;
    jest
      .spyOn(global, "fetch")
      .mockImplementation(
        jest.fn(() =>
          Promise.resolve({ json: () => Promise.resolve(fakeCustomers) })
        ) as jest.Mock
      );
  });

  afterEach(() => {
    global.fetch = originalFetch;
  });

  it("renders customers component", async () => {
    await act(async () => {
      render(
        <BrowserRouter>
          <CustomersPage />
        </BrowserRouter>
      );
    });

    expect(screen.getByPlaceholderText("Search...")).toBeInTheDocument();
    expect(screen.getByText("Alle klanten")).toBeInTheDocument();
  });
});

describe("CustomerDetails", () => {
  let originalFetch: any;

  beforeEach(() => {
    originalFetch = global.fetch;
    jest
      .spyOn(global, "fetch")
      .mockImplementation(
        jest.fn(() =>
          Promise.resolve({ json: () => Promise.resolve(fakeCustomers) })
        ) as jest.Mock
      );
  });

  afterEach(() => {
    global.fetch = originalFetch;
  });

  it("renders CustomerDetailView", async () => {
    await act(async () => {
      render(
        <MemoryRouter initialEntries={["/10"]}>
          <CustomersPage />
        </MemoryRouter>
      );
    });

    expect(screen.getByText("Wiley Bowers")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Johnson Street")).toBeInTheDocument();
    expect(
      screen.getByDisplayValue("bowerswiley@zentix.com")
    ).toBeInTheDocument();
  });

  it("renders and processes changing lastname", async () => {
    render(
      <MemoryRouter initialEntries={["/10"]}>
        <CustomersPage />
      </MemoryRouter>
    );

    await new Promise((r) => setTimeout(r, 200));

    const lastNameInput = screen.getByLabelText("Achternaam");

    expect(lastNameInput).toHaveValue("Bowers");

    await act(async () => {
      fireEvent.change(lastNameInput, {
        target: { value: "Vermeulen" },
      });
    });

    expect(lastNameInput).toHaveValue("Vermeulen");
  });

  it("renders and gives a validation error on wrong email and phone format", async () => {
    render(
      <MemoryRouter initialEntries={["/10"]}>
        <CustomersPage />
      </MemoryRouter>
    );

    await new Promise((r) => setTimeout(r, 200));

    await act(async () => {
      fireEvent.change(screen.getByLabelText("Telefoonnummer"), {
        target: { value: "010-123" },
      });
      fireEvent.change(screen.getByLabelText("Email"), {
        target: { value: "vermeulen@gmail" },
      });
    });

    await act(async () => {
      fireEvent.submit(screen.getByTestId("opslaanCustomerKnop"));
    });

    expect(screen.getByText(StringConstants.EMAIL_REGEX)).toBeInTheDocument();
    expect(
      screen.getByText(StringConstants.PHONENUMBER_REGEX)
    ).toBeInTheDocument();
  });
});
