import { useContext, useMemo } from "react";
import { useParams } from "react-router-dom";
import CustomersContext from "../../context/CustomersContext";
import "./customers.css";
import CustomerData from "./CustomerData";
import CustomerObjects from "./CustomerObjects";
import DocumentUpload from "../document-upload";

const CustomerDetailView = () => {
  const { id } = useParams();
  const { customers } = useContext(CustomersContext);
  const objects = useMemo(
    () => customers.find((c) => c.id?.toString() === id)?.items ?? [],
    [customers, id]
  );

  return (
    <div className="row mt-3">
      {customers
        .filter((c) => c.id?.toString() === id)
        .map((customer, index) => (
          <div key={index} className="col customerdetails">
            <ul className="nav nav-pills mb-3 shadow-sm" id="myTab">
              <li className="nav-item">
                <a
                  href="#personalia"
                  className="nav-link active"
                  data-bs-toggle="tab"
                >
                  Personalia
                </a>
              </li>
              <li className="nav-item">
                <a href="#dossier" className="nav-link" data-bs-toggle="tab">
                  Dossier
                </a>
              </li>
            </ul>
            <div className="tab-content">
              <div className="tab-pane fade show active" id="personalia">
                <CustomerData customer={customer} />
                <CustomerObjects objects={objects} />
              </div>
              <div className="tab-pane fade" id="dossier">
                <DocumentUpload
                  customerId={customer.id}
                  documents={customer.documents}
                            notes={customer.notes}
                            uploadType="customer"
                ></DocumentUpload>
              </div>
            </div>
          </div>
        ))}
    </div>
  );
};

export default CustomerDetailView;
