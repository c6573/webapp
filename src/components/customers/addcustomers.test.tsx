import userEvent from "@testing-library/user-event";
import { render, screen, within } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import AddNewCustomer from "./AddNewCustomer";
import { StringConstants } from "../../StringConstants";

let lastNameTextBox: any;
let firstNameTextBox: any;
let emailTextBox: any;
let phoneNumberTextBox: any;

const testValues = {
  lastName: "De Grote",
  firstname: "Karel",
  email: "k.de.grote@roomserijk.com",
  phoneNumber: "06-12345678",
};

const setup = () => {
  render(
    <BrowserRouter>
      <AddNewCustomer />
    </BrowserRouter>
  );

  lastNameTextBox = screen.getByRole("textbox", { name: /achternaam/i });
  firstNameTextBox = screen.getByRole("textbox", { name: /voornaam/i });
  emailTextBox = screen.getByRole("textbox", { name: /email/i });
  phoneNumberTextBox = screen.getByRole("textbox", { name: /telefoonnummer/i });
};
describe("New customer form", () => {
  beforeEach(() => {});
  it("renders", () => {
    setup();
    expect(screen.getByText("Achternaam")).toBeInTheDocument();
  });
  it("should be empty after load", () => {
    setup();
    expect(
      screen.getByRole("form", {
        name: /add new customer/i,
      })
    ).toHaveFormValues({
      lastName: "",
      firstName: "",
      email: "",
      phoneNumber: "",
    });
  });
  it("should accept input", async () => {
    setup();
    const user = userEvent.setup();
    await user.type(lastNameTextBox, testValues.lastName);
    await user.type(firstNameTextBox, testValues.firstname);
    await user.type(emailTextBox, testValues.email);
    await user.type(phoneNumberTextBox, testValues.phoneNumber);

    expect(
      screen.getByRole("form", {
        name: /add new customer/i,
      })
    ).toHaveFormValues({
      lastName: testValues.lastName,
      firstName: testValues.firstname,
      email: testValues.email,
      phoneNumber: testValues.phoneNumber,
    });
  });
  it("should validate properly and have an error on lastname", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("lastname-error"));
    expect(getByText(StringConstants.LASTNAME_REQUIRED)).toBeInTheDocument();
  });
  it("should validate properly and have an error on firstname", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("firstname-error"));
    expect(getByText(StringConstants.FIRSTNAME_REQUIRED)).toBeInTheDocument();
  });
  it("should validate properly and have an error on email", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("email-error"));
    expect(getByText(StringConstants.EMAIL_REQUIRED)).toBeInTheDocument();
  });
  it("should validate properly and have an error on email regex", async () => {
    setup();
    const user = userEvent.setup();
    await user.type(emailTextBox, "blaat@blabla");

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("email-error"));
    expect(getByText(StringConstants.EMAIL_REGEX)).toBeInTheDocument();
  });
  it("should validate properly and have an error on phonenumber", async () => {
    setup();
    const user = userEvent.setup();

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("phonenumber-error"));
    expect(getByText(StringConstants.PHONENUMBER_REQUIRED)).toBeInTheDocument();
  });
  it("should validate properly and have an error on phonenumber regex", async () => {
    setup();
    const user = userEvent.setup();
    await user.type(phoneNumberTextBox, "061234567");

    const button = screen.getByRole("button", { name: /opslaan/i });
    await user.click(button);

    let { getByText } = within(screen.getByTestId("phonenumber-error"));
    expect(getByText(StringConstants.PHONENUMBER_REGEX)).toBeInTheDocument();
  });
});
