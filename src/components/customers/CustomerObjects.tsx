import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { DataGrid, GridSelectionModel } from "@mui/x-data-grid";
import { useContext, useEffect, useState } from "react";
import { MdDelete, MdEdit } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { IObject } from "../../hooks/Models";
import { DeleteObject } from "../../hooks/useObjects";
import CustomPagination from "../shared/CustomPagination";
import DatagridUtilityBar from "../shared/DatagridUtilityBar";
import { toast } from "react-toastify";
import { CeToastSettings } from "../shared/Toasts";
import { fetchAllCustomers } from "../../hooks/useCustomers";
import CustomersContext from "../../context/CustomersContext";
import { StringConstants } from "../../StringConstants";

const CustomerObjects = (props: { objects: IObject[] }) => {
  const { updateCustomers } = useContext(CustomersContext);
  const navigate = useNavigate();
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [objectRows, setObjectRows] = useState<IObject[]>(props.objects);
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);

  useEffect(() => {
    setObjectRows(props.objects);
  }, [props.objects]);

  const handleOnClickObjectsDataGridRow = (param: any) => {
    navigate("/objects/" + param.id);
  };

  const handleDeleteSelection = () => {
    setOpenConfirmDialog(true);
  };

  const handleConfirmDelete = () => {
    setOpenConfirmDialog(false);
    selectionModel.forEach((value) => {
      DeleteObject(value.toString())
        .then(() => {
          fetchAllCustomers().then((customers) => updateCustomers(customers));
          toast.success(
            `Object ${value} ${StringConstants.DELETE_SUCCEEDED}`,
            CeToastSettings
          );
        })
        .catch((error) => {
          toast.error(error, CeToastSettings);
        });
    });
  };

  const toggleEditMode = (e: any) => {
    e.target.blur();
    setSelectionModel([]);
    setEditMode(!editMode);
  };

  const handleCloseConfirmDialog = () => {
    setOpenConfirmDialog(false);
  };

  const handleFilteredRows = (filteredObjects: IObject[]) => {
    setObjectRows(filteredObjects);
  };

  const data = {
    columns: [
      { field: "type", headerName: "Type", flex: 1, minWidth: 130 },
      {
        field: "licensePlateNumber",
        headerName: "Kenteken",
        flex: 1,
        minWidth: 130,
      },
      { field: "length", headerName: "Lengte (m)", flex: 1, minWidth: 130 },
      {
        field: "bikeCarrier",
        headerName: "Fietsendrager",
        type: "boolean",
        minWidth: 140,
      },
    ],
    rows: objectRows,
  };

  const buttons = [
    {
      icon: <MdDelete />,
      onClick: handleDeleteSelection,
      buttonClasses: "btn-danger",
      disabled: !(editMode && selectionModel.length > 0),
      hide: !editMode,
    },
    {
      icon: <MdEdit />,
      onClick: toggleEditMode,
      buttonClasses: "btn-warning",
    },
  ];

  return (
    <>
      <DatagridUtilityBar
        title="Gekoppelde objecten"
        data={props.objects}
        setFilteredRows={handleFilteredRows}
        buttons={buttons}
      />
      <div className="data-grid detail-page-grid">
        <DataGrid
          {...data}
          disableSelectionOnClick
          disableColumnMenu
          autoPageSize
          rowHeight={44}
          checkboxSelection={editMode}
          onSelectionModelChange={(newSelectionModel) => {
            setSelectionModel(newSelectionModel);
          }}
          selectionModel={selectionModel}
          onRowClick={(params) => handleOnClickObjectsDataGridRow(params)}
          components={{
            Pagination: CustomPagination,
          }}
        />
      </div>
      <Dialog open={openConfirmDialog} onClose={handleCloseConfirmDialog}>
        <DialogTitle>Verwijderen</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Weet u zeker dat u{" "}
            {selectionModel.length === 1 ? "dit object" : "deze objecten"} wilt
            verwijderen?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseConfirmDialog}>Cancel</Button>
          <Button
            variant="contained"
            color="error"
            onClick={handleConfirmDelete}
          >
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default CustomerObjects;
