import { DataGrid } from "@mui/x-data-grid";
import { useContext, useEffect, useState } from "react";
import { SiMicrosoftexcel } from "react-icons/si";
import { useNavigate } from "react-router-dom";
import CustomersContext from "../../context/CustomersContext";
import { ICustomer } from "../../hooks/Models";
import CustomPagination from "../shared/CustomPagination";
import DatagridUtilityBar from "../shared/DatagridUtilityBar";
import * as XLSX from "xlsx";
import "./customers.css";

const Customers = () => {
  const { customers } = useContext(CustomersContext);
  const [rows, setRows] = useState<ICustomer[]>(customers);
  const navigate = useNavigate();

  useEffect(() => {
    setRows(customers);
  }, [customers]);

  const handleOnClickDataGridRow = (param: any) => {
    navigate("/customers/" + param.id);
  };

  const handleFilteredRows = (filteredCustomers: ICustomer[]) => {
    setRows(filteredCustomers);
  };

  const handleOnClickNewCustomer = () => {
    navigate("/customers/add");
  };

  const handleOnClickExportButton = () => {
    const exportData = rows.map((r) => {
      const { items, ...rest } = r;

      return {
        ...rest,
      };
    });

    const ws = XLSX.utils.json_to_sheet(exportData);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "customers");
    XLSX.writeFile(wb, "customers.xlsx");
  };

  function getAddress(params: any) {
    return `${params.row.street || ""} ${params.row.houseNumber || ""}`;
  }

  const data = {
    columns: [
      { field: "lastName", headerName: "Naam", flex: 0.9, minWidth: 130 },
      { field: "firstName", headerName: "Voornaam", flex: 0.9, minWidth: 100 },
      {
        field: "phoneNumber",
        headerName: "Telefoonnummer",
        flex: 1,
        minWidth: 130,
      },
      { field: "email", headerName: "Email", flex: 1, minWidth: 250 },
      {
        field: "Address",
        headerName: "Adres",
        flex: 1,
        minWidth: 170,
        valueGetter: getAddress,
      },
      { field: "city", headerName: "Plaats", flex: 1, minWidth: 120 },
    ],
    rows: rows,
  };

  const buttons = [
    {
      title: "Nieuwe klant",
      onClick: handleOnClickNewCustomer,
      buttonClasses: "btn-primary",
    },
    {
      icon: <SiMicrosoftexcel />,
      onClick: handleOnClickExportButton,
      buttonClasses: "btn-dark excel-icon",
    },
  ];

  return (
    <>
      <DatagridUtilityBar
        title="Alle klanten"
        data={customers}
        setFilteredRows={handleFilteredRows}
        buttons={buttons}
      />
      <div className="col-md-12 data-grid overview-page-grid">
        <DataGrid
          {...data}
          disableSelectionOnClick
          disableColumnMenu
          autoPageSize
          rowHeight={44}
          onRowClick={(params) => handleOnClickDataGridRow(params)}
          components={{
            Pagination: CustomPagination,
          }}
        />
      </div>
    </>
  );
};

export default Customers;
