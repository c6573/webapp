import { Autocomplete, CircularProgress, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import { ICustomer } from "../../hooks/Models";
import { fetchAllCustomers } from "../../hooks/useCustomers";

const CustomerTypeAhead = (props: {
  customer?: ICustomer;
  onCustomerChange: (value: ICustomer | null) => void;
}) => {
  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState<readonly ICustomer[]>([]);
  const [value, setValue] = useState<ICustomer | null>(
    props.customer === undefined ? null : props.customer
  );
  const loading = open && options.length === 0;

  useEffect(() => {
    let active = true;

    if (!loading) {
      return undefined;
    }

    (async () => {
      const allCustomers = await fetchAllCustomers();

      if (active) {
        setOptions(allCustomers);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading]);

  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  const handleChange = (val: ICustomer | null) => {
    props.onCustomerChange(val);
  };

  return (
    <Autocomplete
      id="contactpersoon"
      options={options}
      value={value}
      getOptionLabel={(option: ICustomer) =>
        `${option.firstName} ${option.lastName}`
      }
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      onChange={(_event: any, newValue: ICustomer | null) => {
        setValue(newValue);
        handleChange(newValue);
      }}
      isOptionEqualToValue={(opt, val) => opt.id === val.id}
      loading={loading}
      size="small"
      renderInput={(params) => (
        <TextField
          {...params}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
};

export default CustomerTypeAhead;
