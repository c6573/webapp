import { useContext, useState } from "react";
import { toast } from "react-toastify";
import CustomersContext from "../../context/CustomersContext";
import { ICustomer } from "../../hooks/Models";
import { AddCustomer, fetchAllCustomers } from "../../hooks/useCustomers";
import { StringConstants } from "../../StringConstants";
import { CeToastSettings } from "../shared/Toasts";
import {
  ValidateCustomerFormFields,
  CustomerValidationFields,
} from "./CustomerFormValidation";

const AddNewCustomer = () => {
  const { updateCustomers } = useContext(CustomersContext);
  const defaultValues: ICustomer = {
    firstName: "",
    lastName: "",
    street: "",
    houseNumber: "",
    postalCode: "",
    city: "",
    phoneNumber: "",
    email: "",
    documents: [],
  };
  const errors: CustomerValidationFields = {
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
  };

  const [state, setState] = useState({
    fields: { ...defaultValues },
    errors: errors,
  });

  const handleChange = (e: any) => {
    setState({
      ...state,
      fields: {
        ...state.fields,
        [e.target.name]: e.target.value,
      },
    });
  };

  const handleResetForm = (e: any) => {
    e.preventDefault();
    setState({
      fields: { ...defaultValues },
      errors: { ...errors },
    });
  };

  const handleValidation = () => {
    const validation = ValidateCustomerFormFields(state.fields);
    setState({
      ...state,
      errors: validation.errors,
    });
    return validation.valid;
  };

  const saveNewCustomer = (e: any) => {
    e.preventDefault();
    if (handleValidation()) {
      AddCustomer(state.fields)
        .then((response) => {
          fetchAllCustomers().then((customers) => updateCustomers(customers));
          toast.success(
            `${response.firstName} ${response.lastName} ${StringConstants.SUBMIT_SUCCEEDED}`,
            CeToastSettings
          );
          handleResetForm(e);
        })
        .catch((error) => {
          toast.error(`${StringConstants.SUBMIT_ERROR}`, CeToastSettings);
          console.error(error);
        });
    }
  };

  return (
    <>
      <h3 className="mt-3 mb-3">Toevoegen nieuwe klant</h3>
      <form
        name="newCustomerForm"
        onSubmit={saveNewCustomer}
        aria-label="Add new customer"
      >
        <div className="row mb-3">
          <div className="col-lg-12">
            <fieldset>
              <div className="row mb-2">
                <label htmlFor="lastName" className="col-form-label col-md-3">
                  Achternaam
                </label>
                <div className="col-md-9">
                  <input
                    name="lastName"
                    type="text"
                    className="form-control"
                    id="lastName"
                    value={state.fields.lastName}
                    onChange={handleChange}
                  />
                  <div
                    className="invalid-feedback"
                    data-testid="lastname-error"
                  >
                    {state.errors.lastName}
                  </div>
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="firstName" className="col-form-label col-md-3">
                  Voornaam
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="firstName"
                    className="form-control"
                    id="firstName"
                    value={state.fields.firstName}
                    onChange={handleChange}
                  />
                  <div
                    className="invalid-feedback"
                    data-testid="firstname-error"
                  >
                    {state.errors.firstName}
                  </div>
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="street" className="col-form-label col-md-3">
                  Straat
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="street"
                    className="form-control"
                    id="street"
                    value={state.fields.street}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="row mb-2">
                <label
                  htmlFor="houseNumber"
                  className="col-form-label col-md-3"
                >
                  Huisnummer
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="houseNumber"
                    className="form-control"
                    id="houseNumber"
                    maxLength={10}
                    value={state.fields.houseNumber}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="postalCode" className="col-form-label col-md-3">
                  Postcode
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="postalCode"
                    className="form-control"
                    id="postalCode"
                    pattern="^[0-9]{4}\s?[a-zA-Z]{2}$"
                    value={state.fields.postalCode}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="city" className="col-form-label col-md-3">
                  Plaats
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="city"
                    className="form-control"
                    id="city"
                    value={state.fields.city}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="row mb-2">
                <label htmlFor="email" className="col-form-label col-md-3">
                  Email
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    id="email"
                    value={state.fields.email}
                    onChange={handleChange}
                  />
                  <div className="invalid-feedback" data-testid="email-error">
                    {state.errors.email}
                  </div>
                </div>
              </div>
              <div className="row mb-2">
                <label
                  htmlFor="phoneNumber"
                  className="col-form-label col-md-3"
                >
                  Telefoonnummer
                </label>
                <div className="col-md-9">
                  <input
                    type="text"
                    minLength={10}
                    maxLength={20}
                    name="phoneNumber"
                    className="form-control"
                    id="phoneNumber"
                    value={state.fields.phoneNumber}
                    onChange={handleChange}
                  />
                  <div
                    className="invalid-feedback"
                    data-testid="phonenumber-error"
                  >
                    {state.errors.phoneNumber}
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col d-inline-flex justify-content-end">
            <button
              className="btn btn-secondary me-1"
              onClick={handleResetForm}
            >
              Reset
            </button>
            <button className="btn btn-primary me-1" type="submit">
              Opslaan
            </button>
          </div>
        </div>
      </form>
    </>
  );
};

export default AddNewCustomer;
