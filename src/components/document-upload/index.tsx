import { useEffect, useState, useContext } from "react";
import DatagridUtilityBar from "../shared/DatagridUtilityBar";
import "./document-upload.css";
import { DataGrid, GridSelectionModel } from "@mui/x-data-grid";
import CustomPagination from "../shared/CustomPagination";
import { IDocument, INotes } from "../../hooks/Models";
import { toast } from "react-toastify";
import { CeToastSettings } from "../shared/Toasts";
import CustomersContext from "../../context/CustomersContext";
import { fetchAllCustomers } from "../../hooks/useCustomers";
import { MdCloudDownload, MdDelete, MdEdit } from "react-icons/md";
import {
  DeleteDocument,
  DownloadDocument,
  UploadDocument,
} from "../../hooks/useDocuments";
import { SaveNotes } from "../../hooks/useNotes";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { StringConstants } from "../../StringConstants";
import { fetchAllObjects } from "../../hooks/useObjects";
import ObjectsContext from "../../context/ObjectsContext";

const DocumentUpload = (props: {
  customerId?: number;
  itemId?: number;
  documents: Array<IDocument>;
    notes?: INotes;
    uploadType: string;
}) => {
    const { updateCustomers } = useContext(CustomersContext);
    const { updateObjects } = useContext(ObjectsContext);
  const [selectedFileName, setSelectedFileName] = useState<any>();
  const [selectedFileBase64, setSelectedFileBase64] = useState<any>();
  const [selectionModel, setSelectionModel] = useState<GridSelectionModel>([]);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [notes, setNotes] = useState("");
  const [utilitybar, setUtilitybar] = useState("");
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [documentRows, setDocumentRows] = useState<Array<IDocument>>(
    props.documents
  );

  useEffect(() => {
    setDocumentRows(props.documents);
  }, [props.documents]);

  useEffect(() => {
    if (props.notes?.note) {
      setNotes(props.notes?.note);
    } else {
      setNotes("");
    }
  }, [props.notes?.note]);

  const toggleEditMode = (e: any) => {
    e.target.blur();
    setSelectionModel([]);
    setEditMode(!editMode);
  };

  const handleConfirmDelete = () => {
    setOpenConfirmDialog(false);
    selectionModel.forEach((value) => {
      deleteDocument(value.toString());
    });
  };

  const handleDeleteSelection = () => {
    setOpenConfirmDialog(true);
  };

  const handleCloseConfirmDialog = () => {
    setOpenConfirmDialog(false);
  };

  // On file select (from the pop up)
  function onFileChange(event: any) {
    setSelectedFileName(event.target.files[0].name);

    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const inputData: any = reader.result;
      const replaceValue = inputData?.split(",")[0];
      const base64String = inputData.replace(replaceValue + ",", "");
      setSelectedFileBase64(base64String);
    };
  }

  function onFileUpload() {
    const document: IDocument = {
      id: 0,
      customersId: props.customerId,
      itemId: props.itemId,
      uploadDate: "00-00-0000",
      size: "0kb",
      fileName: selectedFileName,
      fileString: selectedFileBase64,
    };

    UploadDocument(document)
        .then(() => {
        fetchEntities();
        setSelectedFileName(null);
        setSelectedFileBase64(null);
        setUtilitybar(Date.now().toString());
        toast.success(
          `Bestand ${StringConstants.SUBMIT_SUCCEEDED}!`,
          CeToastSettings
        );
      })
      .catch((error) => {
        toast.error(StringConstants.SUBMIT_ERROR, CeToastSettings);
        console.error(error);
      });
  }

  function deleteDocument(id: string) {
    DeleteDocument(id).then((_response) => {
       fetchEntities();
    });
  }

    function downloadDocument(documentToDownload: any) {
        DownloadDocument(documentToDownload.row.id)
      .then((downloadedDocument) => {
          const linkSource = "data:application;base64," + downloadedDocument.fileString;
        const downloadLink = document.createElement("a");
          const fileName = documentToDownload.row.fileName;

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
      })
      .catch((error) => {
        toast.error("Downloaden van bestand is mislukt", CeToastSettings);
        console.error(error);
      });
  }

  const saveNote = () => {
    let body: string;

    if (props.notes) {
      props.notes.note = notes;
      body = JSON.stringify(props.notes);
    } else {
      const note: INotes = {
        id: 0,
        note: notes,
        customerId: props.customerId,
        itemId: props.itemId,
      };
      body = JSON.stringify(note);
    }

    SaveNotes(body)
      .then((_updatedNote) => {
        fetchEntities();
        toast.success(
          `Notitie ${StringConstants.SUBMIT_SUCCEEDED}`,
          CeToastSettings
        );
      })
      .catch((error) => {
        toast.error(StringConstants.SUBMIT_ERROR, CeToastSettings);
        console.error(error);
      });
    };

    const fetchEntities = () => {
        if (props.uploadType === 'customer') {
            fetchAllCustomers().then((customers) => updateCustomers(customers));
        } else {
            fetchAllObjects().then((objects) => updateObjects(objects));
        }
    }

  const data = {
    columns: [
      { field: "fileName", headerName: "Bestandsnaam", flex: 1, minWidth: 130 },
      {
        field: "uploadDate",
        headerName: "Datum geupload",
        flex: 1,
        minWidth: 130,
      },
      { field: "size", headerName: "Grootte", flex: 1, minWidth: 130 },
      {
        field: "download",
        headerName: "Download",
        flex: 1,
        minwidth: 130,
        renderCell: (param: any) => {
          return (
            <a
              className="data-grid-action"
              onClick={() => downloadDocument(param)}
            >
              <MdCloudDownload className="nav-icon" />
            </a>
          );
        },
      },
    ],
    rows: documentRows,
  };

  const handleFilteredRows = (filteredDocuments: any[]) => {
    setDocumentRows(filteredDocuments);
  };

  const buttons = [
    {
      title: "Upload!",
      onClick: onFileUpload,
      buttonClasses: "btn-primary btn-sm",
      disabled: !selectedFileName,
      hide: !editMode,
    },
    {
      icon: <MdDelete />,
      onClick: handleDeleteSelection,
      buttonClasses: "btn-danger",
      disabled: !(editMode && selectionModel.length > 0),
      hide: !editMode,
    },
    {
      icon: <MdEdit />,
      onClick: toggleEditMode,
      buttonClasses: "btn-warning",
    },
  ];

  const input = {
    type: "file",
    classes: "form-control",
    onChange: onFileChange,
    hide: !editMode,
  };

  return (
    <div>
      <div>
        <div className="row mt-3 mb-3">
          <div className="col-md-6">
            <h3>Opmerkingen / bijzonderheden</h3>
          </div>
          <div className="col-md-6 d-inline-flex justify-content-end">
            <button className="btn btn-primary btn-sm me-1" onClick={saveNote}>
              Opslaan
            </button>
          </div>
        </div>
        <textarea
          className="form-control notes"
          value={notes}
          onChange={(note) => setNotes(note.target.value)}
        ></textarea>
      </div>
      <div>
        <DatagridUtilityBar
          key={utilitybar}
          title="Gekoppelde bestanden"
          data={props.documents}
          setFilteredRows={handleFilteredRows}
          buttons={buttons}
          input={input}
        />
        <div className="data-grid detail-page-grid">
          <DataGrid
            {...data}
            disableSelectionOnClick
            disableColumnMenu
            autoPageSize
            rowHeight={44}
            checkboxSelection={editMode}
            onSelectionModelChange={(newSelectionModel) => {
              setSelectionModel(newSelectionModel);
            }}
            selectionModel={selectionModel}
            components={{
              Pagination: CustomPagination,
            }}
          />
        </div>
        <Dialog open={openConfirmDialog} onClose={handleCloseConfirmDialog}>
          <DialogTitle>Verwijderen</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Weet u zeker dat u{" "}
              {selectionModel.length === 1 ? "dit bestand" : "deze bestanden"}{" "}
              wilt verwijderen?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseConfirmDialog}>Cancel</Button>
            <Button
              variant="contained"
              color="error"
              onClick={handleConfirmDelete}
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
};

export default DocumentUpload;
