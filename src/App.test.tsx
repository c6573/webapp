import { render } from '@testing-library/react';
import App from './pages/main-page';

it('renders main container', () => {
  const { container } = render(<App />);
  const mainElement = container.getElementsByTagName("main");
  expect(mainElement).toHaveLength(1);
});

it('renders sidebar container', () => {
  const { container } = render(<App />);
  const sidebarElement = container.getElementsByClassName("sidebar");
  expect(sidebarElement).toHaveLength(1);
});

it('renders divider container', () => {
  const { container } = render(<App />);
  const dividerElement = container.getElementsByClassName("divider");
  expect(dividerElement).toHaveLength(1);
});