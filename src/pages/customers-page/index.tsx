import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import Customers from "../../components/customers";
import AddNewCustomer from "../../components/customers/AddNewCustomer";
import CustomerDetailView from "../../components/customers/CustomerDetailView";
import CustomerNavbar from "../../components/customers/CustomerNavbar";
import LoadingIndicator from "../../components/shared/LoadingIndicator";
import CustomersContext from "../../context/CustomersContext";
import useCustomers from "../../hooks/useCustomers";

const CustomersPage = () => {
  const { data, loading } = useCustomers();
  const [state, setState] = useState(data);

  useEffect(() => {
    setState(data);
  }, [data]);

  if (loading) {
    return <LoadingIndicator />;
  }

  return (
    <>
      <CustomersContext.Provider
        value={{ customers: state, updateCustomers: setState }}
      >
        <Routes>
          <Route path="/" element={<Customers />} />
          <Route
            path="/:id"
            element={
              <>
                <CustomerNavbar />
                <CustomerDetailView />
              </>
            }
          />
          <Route path="/add" element={<AddNewCustomer />} />
        </Routes>
      </CustomersContext.Provider>
    </>
  );
};

export default CustomersPage;
