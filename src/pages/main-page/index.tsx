import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import "./main-page.css";
import Sidebar from "../../components/sidebar";
import NoMatch from "./no-match";
import CustomersPage from "../customers-page";
import EmailTemplatePage from "../email-template-page";
import ObjectsPage from "../objects-page";
import { CeToastContainer } from "../../components/shared/Toasts";

function App() {
  return (
    <BrowserRouter>
      <main className="container-fluid">
        <div className="row flex-nowrap">
          <div className="sidebar p-0 bg-light">
            <Sidebar />
          </div>
          <div className="divider" />
          <div className="col full-height">
            <CeToastContainer />
            <Routes>
              <Route path="/customers/*" element={<CustomersPage />} />
              <Route path="/objects/*" element={<ObjectsPage />} />
              <Route path="/email-template/*" element={<EmailTemplatePage />} />
              <Route path="/" element={<Navigate to="/customers" replace />} />
              <Route path="*" element={<NoMatch />} />
            </Routes>
          </div>
        </div>
      </main>
    </BrowserRouter>
  );
}

export default App;
