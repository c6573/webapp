const NoMatch = () => {
    return (
        <div>
            <div className="row mt-3">
                <h2 className="col-md-12">Pagina niet gevonden!</h2>
            </div>
            <hr />
            <div className="row">
                <div className="col-md-12">
                    <p>
                        De opgevraagde pagina kan niet worden gevonden.
                    </p>
                </div>
            </div>
        </div>
    );
};

export default NoMatch;