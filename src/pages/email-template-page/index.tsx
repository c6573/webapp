import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import DynamicEmailEditor from "../../components/emailEditor";
import LoadingIndicator from "../../components/shared/LoadingIndicator";
import EmailTemplateContext from "../../context/EmailTemplateContext";
import useEmailTemplates from "../../hooks/useEmailTemplates";

const EmailTemplatePage = () => {
  const { data, loading } = useEmailTemplates();
  const [state, setState] = useState(data);

  useEffect(() => {
    setState(data);
  }, [data]);

  if (loading) {
    return <LoadingIndicator />;
  }

  return (
    <div>
      <EmailTemplateContext.Provider value={state}>
        <Routes>
          <Route path="/" element={<DynamicEmailEditor />} />
        </Routes>
      </EmailTemplateContext.Provider>
    </div>
  );
};

export default EmailTemplatePage;
