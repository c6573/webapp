import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import Objects from "../../components/objects";
import AddNewObject from "../../components/objects/AddNewObject";
import ObjectDetailView from "../../components/objects/ObjectDetailView";
import ObjectNavbar from "../../components/objects/ObjectNavbar";
import LoadingIndicator from "../../components/shared/LoadingIndicator";
import ObjectsContext from "../../context/ObjectsContext";
import useObjects from "../../hooks/useObjects";

const ObjectsPage = () => {
  const { data, loading } = useObjects();
  const [state, setState] = useState(data);

  useEffect(() => {
    setState(data);
  }, [data]);

  if (loading) {
    return <LoadingIndicator />;
  }

  return (
    <>
      <ObjectsContext.Provider
        value={{ objects: state, updateObjects: setState }}
      >
        <Routes>
          <Route path="/" element={<Objects />} />
          <Route
            path="/:id"
            element={
              <>
                <ObjectNavbar />
                <ObjectDetailView />
              </>
            }
          />
          <Route path="/add" element={<AddNewObject />} />
        </Routes>
      </ObjectsContext.Provider>
    </>
  );
};

export default ObjectsPage;
