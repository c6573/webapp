export class StringConstants {
    public static TYPE_REQUIRED = "Type dient ingevuld te zijn";
    public static LENGTH_REQUIRED = "Lengte dient ingevuld te zijn";
    public static OWNER_REQUIRED = "U moet een eigenaar selecteren";
    public static LASTNAME_REQUIRED = "Achternaam dient ingevuld te zijn";
    public static FIRSTNAME_REQUIRED = "Voornaam dient ingevuld te zijn";
    public static EMAIL_REQUIRED = "Email dient ingevuld te zijn";
    public static EMAIL_REGEX = "Het email adres is niet geldig";
    public static PHONENUMBER_REQUIRED = "Telefoonnummer dient ingevuld te zijn";
    public static PHONENUMBER_REGEX = "Het telefoonnummer is niet geldig";
    public static SUBMIT_ERROR = "Er is een fout opgetreden bij het opslaan";
    public static SUBMIT_SUCCEEDED = "is opgeslagen";
    public static DELETE_SUCCEEDED = "is verwijderd";
    public static CHANGE_OWNER_ERROR = "Deze klant is al gekoppeld. Verwijder deze eerst.";
    public static DELETE_OWNER_ERROR = "Een primaire klant kan niet verwijderd worden van het object. Indien u deze wilt verwijderen, verander dan eerst de primaire contactpersoon.";
}

