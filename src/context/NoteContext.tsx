import { createContext } from "react";

export type NoteContextType = {
  SaveNotes: any;
};

const NoteContext = createContext<NoteContextType>({
  SaveNotes: (_body: any): void => console.warn("no note provided"),
});

export default NoteContext;
