import { createContext } from "react";
import { IEmailTemplate } from "../hooks/Models";


const EmailTemplateContext = createContext([] as IEmailTemplate[]);

export default EmailTemplateContext;