import { createContext } from "react";
import { ICustomer } from "../hooks/Models";

export type CustomerContextType = {
  customers: ICustomer[];
  updateCustomers: any;
};

const CustomersContext = createContext<CustomerContextType>({
  customers: [],
  updateCustomers: (_customer: ICustomer): void =>
    console.warn("no customer provider"),
});

export default CustomersContext;
