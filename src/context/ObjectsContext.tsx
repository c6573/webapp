import { createContext } from "react";
import { IObject } from "../hooks/Models";

export type ObjectContextType = {
  objects: IObject[];
  updateObjects: any;
};

const ObjectsContext = createContext<ObjectContextType>({
  objects: [],
  updateObjects: (_customer: IObject): void =>
    console.warn("no object provider"),
});

export default ObjectsContext;
