import { IDocument } from "../hooks/Models";
import { createContext } from "react";

export type DocumentContextType = {
    UploadDocument: any;
    DeleteDocument: any;
    DownloadDocument: any;
};

const DocumentContext = createContext<DocumentContextType>({
    
    UploadDocument: (_document: IDocument): void =>
        console.warn("no document provided"),

    DeleteDocument: (_document: IDocument): void =>
        console.warn("no document provided"),

    DownloadDocument: (_documentId: number): void =>
        console.warn("no id has been provided")
});

export default DocumentContext;